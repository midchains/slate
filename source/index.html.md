--- 

title: Ousoul API Documentation

language_tabs: 
   - python: Python
   - javascript: JavaScript
   - java: Java
   - fix: FIX

toc_footers: 
   - <a href="https://ousoul.com/">Sign Up to Ousoul</a> 
   

# includes: 
#    - errors 

search: true 

--- 

# INTRODUCTION 

## Overview

Welcome to Ousoul API documentation. These documents outline trading platform functionality, market details, and APIs.

APIs are separated into two categories: Market data feed and trading. Feed APIs provide market data and are public. Trading APIs require authentication and provide access to placing orders and other account information. 

**Version:** 1.0.0 

## Rate Limits

The Ousoul API is rate limited to prevent abuse that would degrade our ability to maintain consistent API performance for all users.

### WebSocket API

&nbsp;&nbsp;&nbsp;&nbsp;**Public Enpoints**

&nbsp;&nbsp;&nbsp;&nbsp;We throttle public endpoints by IP: 3 requests per second, up to 6 requests per second in bursts. Some endpoints may have custom rate limits.

&nbsp;&nbsp;&nbsp;&nbsp;**Private Endpoints**

&nbsp;&nbsp;&nbsp;&nbsp;We throttle private endpoints by user ID: 5 requests per second, up to 10 requests per second in bursts. Some endpoints may have custom rate limits.

### Financial Information Exchange API

The FIX API throttles the number of incoming messages to 10 commands per second by default. This can be increased as needed.

## Time Format

```json

"time": "1614331055000"

```

The Ousoul API timestamp fields us the Epoch/POSIX format for WebSocket API Requests. Unix and POSIX measure time as the number of seconds that have passed since 1 January 1970 00:00:00 UTC

Do not provide miliseconds. '1614331055' or '1614331055000' are supported. '1614331055123' is not supported.


## Changelog

Recent changes and additions to Ousoul API.

### 2021-01-30

Initial Version

### 2021-09-21

Sandbox and Production - 'querypendingpos' endpoint now available. Details in Account Balance Section.

Sandbox - 'trade' feed will have new 'maker_side' field. Values will be 'B' or 'S'.

Sandbox - 'book' feed will have 'side' message an act:R messages that update when an order is removed from the order book.

### 2021-09-25

Production - 'trade' feed will have new 'maker_side' field. Values will be 'B' or 'S'.

Production - 'book' feed will have 'side' message an act:R messages that update when an order is removed.

### 2021-12-07

Sandbox - new 'openorderquery' endpoint now available , Details in Order & Trade Query Section.

# Supported Orders

The table below details what Order Types and associated Time In Force are supported over the WS API.

| Order Type      | Time In Force    |
| -----------     | ---------------- |
| Limit           | GTC <br/> IOC <br/> FOK <br/> GTT |
| Market          | IOC <br/> FOK |
| Stop Market     | IOC <br/> FOK |
| Stop Limit      | GTC <br/> IOC <br/> FOK |
| Post/Maker Only | GTC |

<br/>

The table below details what Order Types and associated Time In Force are supported over the FIX API.

| Order Type  | Time In Force    |
| ----------- | ---------------- |
| Limit       | GTC <br/> IOC <br/> FOK <br/> GTT |
| Market      | IOC <br/> FOK |

<br/>

`We Only Support Market Sell Order Over FIX API`

# <br/>

# FIX API

The information in Ousoul FIX API specification describes the adaptation of the standard FIX 4.2 for vendors and subscribers to communicate with the Ousoul quotation and execution platform. FIX 4.2 tags, as described in detail on the Financial Information Exchange Protocol Committee website, www.fixprotocol.org as well as custom tags are used extensively in this document and the reader should familiarize themselves with the latest updates to this release. If an application message in Financial Information Exchange Protocol version 4.2, or previous FIX versions, is not included in this document, the message is ignored.


# Connectivity
To connect to Ousoul over the FIX API clients must establish a secure connection to the FIX gateway. you can Connect to the FIX gateway with secure tcp+ssl connection without whitelisting your ip. For a FIX implementation that does not support establishing a TCP SSL connection natively, you will need to setup a local proxy such as stunnel to establish a secure connection to the FIX gateway. if these two ways not working with you then you to provide us connectivity details such as the IP Address and Port Number which will be assigned by the Ousoul trade operations team. To request these details and to Whitelist clients' IP addresses, clients can contact support@Ousoul.com or raise a request via the support portal through the www.ousoul.com website.

<i>Message Sequence Numbers are reset daily at 01:00 UTC.</i>

For testing use;

IP Address: `fix-sandbox.ousoul.com`<br />
Port: `44323`


For access to Market Data over FIX use;

Port: `44325`


# FIX Order Placement

The trading platform will respond to any new order messages and exution with execution reports. Example execution report responses are available below.

<br/>

This section outlines the FIX messages, how they are supported, and to what extent the business data is translated within the FIX Gateway.

### Message Header

<br/>

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
| 8    | BeginString             | Yes       | FIX.4.2 |
| 9    | BodyLength              | Yes       | (Always unencrypted, must be second field in message)      |
| 35   | MsgType                 | Yes       |  (Always unencrypted, must be third field in message)    |
| 49   | SenderCompID            | Yes       | Provided by Ousoul |
| 56   | TargetCompID            | Yes       | Provided by Ousoul |
| 34   | MsgSeqNum               | Yes       | (Can be embedded within encrypted data section.)     |
| 52   | SendingTime             | Yes       |  Required    |


### Message Trailer

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
| 10   | CheckSum                | Yes       | (Always unencrypted, always last field in message)   |


## Login (A)

```fix
Example Message

Client
8=FIX.4.2^A9=70^A35=A^A34=2^A49=SENDERCOMP^A52=20210714-01:01:10.569^A56=MIDCDEV1^A98=0^A108=60^A10=049^A

Gateway
8=FIX.4.2^A9=72^A35=A^A49=MIDCDEV1^A56=YOURSENDCOMD^A34=422^A52=20210714-07:52:46.306^A98=0^A108=60^A10=163^A
```

The logon message identifies and authenticates the user or member and establishes a connection to the FIX Gateway. 
The FIX gateway accepts Logon messages as per the FIX specification. Further, the FIX gateway supports the logon sequence required for session authentication. 
After a successful logon as described in the specification the FIX gateway will: 
Initiate retransmission processing via a resend request if the Logon sequence number is greater than the value expected
Initiate logout processing via a Logout message with an appropriate error message, then waits for a confirming Logout before disconnecting if the Logon sequence number is less than expected. If the confirming Logout has not been received within a short period of time the session will be disconnected.
Handle retransmission requests.
Initiate a Logon using the SenderCompID in the message header.
Will forwarded to the FIX client messages that are waiting in the outbound queue
Begin regular message communication.
<br/>

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
|     | Standard Header             | Yes    |MsgType = A  |
| 108    | HeartBtInt             | Yes       |  |
|     | Standard Trailer             | Yes       |  |



## Logout (5)

```fix
Example Message


Client
8=FIX.4.2^A9=115^A35=5^A34=496^A49=YOURSENDCOMP^A52=20210714-08:21:57.076^A56=MIDCDEV1^AA10=128^A
```

The Logout message initiates or confirms the termination of a FIX session. 

The FIX gateway will receive and generate logout messages as required by the FIX Protocol. The gateway follows the prescribed sequence of messages for the proper termination of the session. 
Messages received by the gateway after the client logs out are stored in a log file for transmission to the client once the client logs in again within the same trading day. The messages to be transmitted are dependent on the sequence number reconciliation that occurs on a logon handshake. 

Upon receipt of a Logout message: 
<br/>
1. A confirming logout message will be sent by the gateway to the client; then,
<br/>
2. The session will be disconnected. The FIX gateway will not initiate a logoff except when a severe error has occurred.

<br/>

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
|     | Standard Header             | Yes       |  |
| 58    | Text             | No       | Free format text string (Note: this field does not have a specified maximum length) If the Logout message has been sent by the the FIX gateway, then this field will contain the text “Session closed”. |
|     | Standard Trailer             | Yes       |  |




## Heartbeat (0)

```fix
Example Message

Client
8=FIX.4.2^A9=60^A35=0^A34=336^A49=YOURSENDCOMP^A52=20210714-06:36:18.136^A56=MIDCDEV1^A10=120^A

Gateway
8=FIX.4.2^A9=58^A35=0^A49=MIDCDEV1^A56=YOURSENDCOMP^A34=8^A52=20210714-01:07:40.546^A10=020^A
```

This section indicates the message intended to monitor the status of the communications link during periods of inactivity. 

The FIX market data accepts and generates Heartbeat messages as per the FIX specification.

• Inbound: Handled as specified 
• Outbound: In response to a test request or timeout. 
• Response: None

The heartbeat message should be sent if agreed upon Heartbeatinterval has elapsed since the last message sent. If any 
proceeding Heartbeatinterval a Heartbeat message need not be sent
<br/>

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
|      | Standard Header         | Yes       | MsgType = 0 |
|      | Standard Trailer        | Yes       |      |





## New Order - Single (D)

```fix
Example Message

Client - LTCUSD Buy Limit GTC Order for OrderQty 0.01 at Price of 150
8=FIX.4.2^A9=160^A35=D^A34=569^A49=YOURSENDCOMP^A52=20210714-10:31:02568^A56=MIDCDEV1^A11=43637618552482556000^A21=2^A38=1^A40=2^A44=150^A54=1^A55=LTCUSD^A59=1^A60=20210714-10:31:02.552^A43229=10^A10=192^A



Gateway - Execution Reports

ER NEW
8=FIX.4.2^A9=230^A35=8^A34=582^A49=MIDCDEV1^A52=20210714-10:31:03.862^A56=YOURSENDCOMP^A6=0.0^A11=43637618552482556000^A14=0^A17=30005_582^A20=0^A37=62XGZUU7W05SC3^A38=1000^A39=0^A40=2^A44=150.0^A54=1^A55=LTCUSD^A59=1^A60=20210714-10:31:03.858^A150=0^A151=1000^A43229=10000^A10=132^A

ER FILL
8=FIX.4.2^A9=300^A35=8^A34=583^A49=MIDCDEV1^A52=20210714-10:31:04.106^A56=YOURSENDCOMP^A6=141.78^A11=43637618552482556000^A14=1000^A17=62XGZUU7W05SC32_62XGZUU7W05T^A20=0^A31=141.78^A32=1000^A37=62XGZUU7W05SC3^A38=1000^A39=2^A40=2^A44=150.0^A54=1^A55=LTCUSD^A59=1^A60=20210714-10:31:03.935^A150=2^A151=0^A43211=CROX^A43212=MIDC^A43214=T^A43229=10000^A10=105^A
```

This message is used to submit an order to the trading system for processing. The trading platform will respond with execution reports.

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
|     | Standard Header             | Yes       |  |
| 11    | ClOrdId             | Yes       | Unique identifier of the order. Must be unique for each session, max 32 chars. |
| 1    | Account             | No       | Optional identifier from customer, will be passed back in Execution Report. |
| 21    | HandlInst          | Yes      | '1' = Automated execution order, private, no Broker intervention '2' = Automated execution order, public, Broker intervention OK  '3' = Manual order, best execution   |
| 55    | Symbol             | Yes       | Common, “human understood” representation of the security, bitcoins US Dollar – BTCUSD, etc. |
| 54    | Side           | Yes       | '1' = Buy '2' = Sell |
| 38    | OrderQty           | Yes       | Size of the order. E.G. 10  |
| 43229 | FractionBase             | Yes       | If FractionBase is 100 and OrderQty field = 1, that means the Order Qty sent to the platform is 0.01. |
| 40    | OrdType             | Yes       | '1' = Market '2' = Limit |
| 44    | Price             | No       | Required for limit Ordtypes  |
| 59 | TimeInForce | No | '1' = GTC '3' = IOC '4' = FOK '6' = GTD (ExpireTime must be set in datetime format)   |
| 60 | TransactTime | No | Time of execution/order creation (UTC Time in datetime format) |
| 126 | ExpireTime | No | Required if TimeInForce = 6 (UTC Time in datetime format - 17:18:02.54)
|     | Standard Trailer             | Yes       |  |




## Order Cancel Request (F)

```fix
Example Message

Client
8=FIX.4.2^A9=188^A35=F^A34=104^A49=HASFIX02^A52=20210714-12:56:35.070^A56=MIDCDEV1^A11=63637618640333706000^A21=1^A38=1^A40=2^A41=62637618638809898000^A44=125^A54=1^A55=LTCUSD^A59=1^A60=20210714-12:56:35.075^A43229=100^A10=196^A

Gateway
8=FIX.4.2^A9=183^A35=F^A34=724^A49=MIDCDEV1^A52=20210714-12:56:35.080^A56=YOURSENDCOMP^A11=63637618640333706000^A21=2^A38=1^A40=2^A41=62637618638809898000^A44=125^A54=1^A55=LTCUSD^A59=1^A60=20210714-12:56:35.80^A43229=1^A10=094^A

```

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
|     | Standard Header             | Yes       |  |
| 41    | OrigClOrdId             | Yes       | ClOrdID of the previous order |
| 37    | OrdId             | No       | Unique Identifier of order assigned by the platform. |
| 11    | ClOrdId             | Yes       | Unique identifier of the order. Must be unique for each session, max 32 chars. |
| 55    | Symbol             | Yes       | Common, “human understood” representation of the security, bitcoins US Dollar – BTCUSD, etc. |
| 54    | Side           | Yes       | '1' = Buy '2' = Sell |
| 38    | OrderQty           | Yes       | Size of the order. E.G. 10  |
| 58 | Text             | No       |  |
|     | Standard Trailer             | Yes       |  |




## Order Cancel/Replace Request (G)

```fix
Example message

Client
8=FIX.4.2^A9=188^A35=F^A34=104^A49=HASFIX02^A52=20210714-12:37:23.030^A56=MIDCDEV1^A11=61637618630042074000^A21=1^A38=1^A40=2^A41=60637618629492018000^A44=47250^A54=1^A55=LTCUSD^A59=1^A60=20210714-12:37:23.35^A43229=100^A10=196^A

Gateway
8=FIX.4.2^A9=183^A35=G^A34=704^A49=MIDCDEV1^A52=20210714-12:37:23.035^A56=YOURSENDCOMP^A11=61637618630042074000^A21=2^A38=1^A40=2^A41=60637618629492018000^A44=47250^A54=1^A55=LTCUSD^A59=1^A60=20210714-12:37:23.35^A43229=1^A10=057^A
```
The Execution Report is automatically generated and sent by the gatway automatically.

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
|      | Standard Header         | Yes       |      |
| 41   | OrigClOrdId             | Yes       | ClOrdID of the previous order |
| 37   | OrdId                   | No        | Unique Identifier of order assigned by the platform. |
| 11   | ClOrdId                 | Yes       | Unique identifier of the order. Must be unique for each session, max 32 chars. |
| 1    | Account                 | No        | Optional identifier from customer, will be passed back in Execution Report. |
| 55   | Symbol                  | Yes       | Common, “human understood” representation of the security, bitcoins US Dollar – BTCUSD, etc. |
| 54   | Side                    | Yes       | '1' = Buy '2' = Sell |
| 38   | OrderQty                | Yes       | Size of the order. E.G. 10  |
| 40   | OrdType                 | Yes       |       |
| 44   | Price                   | No        | Required for limit Ordtypes  |
| 59   | TimeInForce             | No        | '3' = IOC (Immediate-Or-Cancel) |
| 126  | ExpireTime              | No        | Required if TimeInForce = GTT (UTC time in datetime format - 17:18:02.544)
|      | Standard Trailer        | Yes       |       |


## Execution Report (8)

The Execution Report is automatically generated and sent by the gatway automatically.


| Tag   | Field Name             | Required  | Note |
| ----  | ---------------------- | --------  | ---- |
|       | Standard Header        | -         | |
| 37    | OrdId                  | -         | Unique Identifier of order assigned by the platform. |
| 6     | AvgPx                  | -         | |
| 11    | ClOrdId                | -         | Unique identifier of the order copied from customer's request. |
| 41    | OrigClOrdId            | -         | ClOrdID of the previous order |
| 14    | CumQty                 | -         | |
| 17    | ExecID                 | -         | Platform assigned execution ID |
| 20    | ExecTransType          | -         | 0 |
| 32    | LastQty                | -         | |
| 37    | OrderID                | -         | |
| 38    | OrderQty               | -         | Size of the order. E.G. 10|
| 39    | OrdStatus              | -         | 0 = New, 1 = PartiallyFilled, 2 = Filled, 4 = Canceled, 5 = Replaced, 6 = PendingCancel, 8 = Rejected, A = PendingNew, C = Expired, E = PendingReplace |
| 40    | OrdType                | -         | '1' = Market '2' = Limit |
| 44    | Price                  | -         | |
| 54    | Side                   | -         | '1' = Buy  '2' = Sell |
| 55    | Symbol                 | -         | Common, “human understood” representation of the trading pair, bitcoins US Dollar – BTCUSD, etc. |
| 59    | TimeInForce            | -         | |
| 60    | TransactTime           | -         | |
| 126   | ExpireTime             | -         | Time order will expire |
| 150   | ExecType               | -         | |
| 151   | LeavesQty              | -         | |
| 43211 | Execution Venue        | -         | This value can be ignored, will be CROX on FILLS |
| 43212 | Execution Contra Broker| -         | This value can be ignored, will be MIDC on FILLS |
| 43214 | Liquidity Indicator    | -         | ‘P’, provided liquidity ‘T’, took liquidity |
| 43229 | Fraction Base          | -         | The amount to divide qty by to get real value |
| 58    | Text                   | -         | |
|       | Standard Trailer       | -         | |

## Order Cancel Reject (9)
This message is send when a Order Cancel Request or Order Cancel/Replace Request is rejected

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
|      | Standard Header         | -         |      |
| 41   | OrigClOrdId             | -         | ClOrdID of the previous order. |
| 37   | OrdId                   | -         | Unique Identifier of order assigned by the platform. |
| 11   | ClOrdId                 | -         | Unique identifier of the order. Must be unique for each session, max 32 chars. |
| 39   | OrdStatus               | -         | OrdStatus value after this cancel reject is applied. |
| 102  | CxlRejReason            | -         | Code to identify reason for cancel rejection. |
| 58   | Text                    | -         | Optional explanation message. |
| 434  | CxlRejResponseTo        | -         | Unique identifier of the Canceled order. |
| 17   | ExecID                  | -         | Execution id. |
|      | Standard Trailer        | -         |      |

# FIX Market Data

The information in Ousoul FIX API specification describes the adaptation of the standard FIX 4.2 for vendors and subscribers to communicate with the Ousoul quotation and execution platform. FIX 4.2 tags, as described in detail on the Financial Information Exchange Protocol Committee website, www.fixprotocol.org as well as custom tags are used extensively in this document and the reader should familiarize themselves with the latest updates to this release. If an application message in Financial Information Exchange Protocol version 4.2, or previous FIX versions, is not included in this document, the message is ignored.

**After you connect to a market data session you will automatically receive msg type X and W for all available trading pairs.**


## Market Data Responses – Top of Book Snapshot (W)

```fix
Example FIX Message

The FIX message would appear as 

8=FIX.4.2^A9=103^A35=W^A34=54^A49=MIDCDEV1^A52=20210713-13:49:37^A56=YOURSENDCOMP^A55=LTCUSD^A262=0^A268=1^A269=2^A270=133.54^A271=0.91^A10=024^A

```

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
|      | Standard Header         |        | MsgType = W |
| 262  | MDReqID              |        | The MDReqID of the MarketDataRequest message. |
| 55   | Symbol             |        | Identifier for the symbol  |
| 268  | NoMDEntries         |        | Number of market data entries in this snapshot. |
| 269  | MDEntryType           |        | Type of market data entry. Valid values: 0 = Bid, 1 = Offer  |
| 270  | MDEntryPx            |        | Price of the market data entry. |
| 271  | MDEntrySize         |        | Qty represented by the Market Data Entry. |
|     | Standard Trailer             |        |  |






## Market Data Responses– Incremental Refresh (X)

```fix
Example FIX Message

The FIX message would appear as 

8=FIX.4.8=FIX.4.2^A9=146^A35=X^A34=77^A49=MIDCDEV1^A52=20210713-14:06:54^A56=YOURSENDCOMP^A262=0^A268=1^A279=0^A269=0^A278=247697979505377327^A55=LTCUSD^A270=120^A271=0.1^A58=62WM7F95YBJTC3^A10=075^A

8=FIX.4.2^A9=146^A35=X^A34=77^A49=MIDCDEV1^A52=20210713-8=FIX.4.2^A9=146^A35=X^A34=78^A49=MIDCDEV1^A52=20210713-14:07:21^A56=YOURSENDCOMP^A262=0^A268=1^A279=0^A269=0^A278=247697979505377328^A55=LTCUSD^A270=121^A271=0.2^A58=62WM7F95YBJUC3^A10=075^A

```
<br/>

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
|      | Standard Header         |        | MsgType = X |
| 262  | MDReqID              |        | The MDReqID of the MarketDataRequest message. |
| 268  | NoMDEntries         |        | Number of market data entries in this snapshot. |
| 279  | MDUpdateAction      |        | The Market Data update action type. It must be the first field in the repeating group. The only valid values are: 0 = New, 2 = Delete |
| 269  | MDEntryType           |        | Type of market data entry. Valid values: 0 = Bid, 1 = Offer, 2=Trade |
| 278  | MDEntryID        |        | Market data identifier |
| 55   | Symbol  |  | Identifier for the symbol |
| 65   | SymbolSfx |  |  |
| 270  | MDEntryPx  |   | Price of the market data entry. |
| 271  | MDEntrySize         |        | Number of shares represented by the Market Data Entry. |
| 58   | Text         |        | Order ID reference |
|      | Standard Trailer             |        |  |



## Market Data Request Reject (Y)

<br/>

| Tag  | Field Name              | Required  | Note |
| ---- | ----------------------- | --------  | ---- |
|      | Standard Header         |        | MsgType = Y |
| 262  | MDReqID              |      | The MDReqID of the MarketDataRequest message.|
| 268  | Text         |        | Reason for the rejection.|
|      | Standard Trailer      |        |  | 

# <br/>

# WEBSOCKET API

# Connectivity

> Example Structure.

```python
from websocket import create_connection

ws = create_connection("wss://sandbox.ousoul.com/ws/")
ws.send('''
   {
      "type": "TYPE",
      "request": "REQUEST"
   }
''')

print(ws.recv())

ws.close()

```

```javascript
const WebSocket = require('ws');
const ws = new WebSocket("wss://sandbox.ousoul.com/ws/")

ws.onopen = function() { 
        ws.send(JSON.stringify(
           {
              "type": "TYPE",
              "request": "REQUEST"
            }
        ))

        ws.onmessage = function(response) {
            let websocket_response = JSON.parse(response.data);
            
            console.log(websocket_response)
        }
         ws.close()
    }
}

```

The WebSocket market data feed provides real-time market data updates for orders and trades.

`wss://websocket.ousoul.com/ws/`

For testing, use:

`wss://sandbox.ousoul.com/ws/`

The WebSocket feed uses a bidirectional protocol, which encodes all messages as JSON objects. All messages have a type attribute that can be used to handle the message appropriately.

Please note that new message types can be added at any point in time. Clients are expected to ignore messages they do not support.

# Market Data


## Get Symbols


```json
# Request

   {
      "type": "getsymbols"
   }

# Response

   {
      "result":"OK",
      "data":[
         {
            "security":"USD",
            "fractionbase":100,
            "tradingsymbol":false,
            "fiat":false,
            "pair":false
         },
         {
            "security":"LTCUSD",
            "fractionbase":10000,
            "pair_first":"LTC",
            "tradingsymbol":true,
            "fiat":false,
            "pair_second":"USD",
            "pair":true
         },
         {
            "security":"LTC",
            "fractionbase":10000,
            "tradingsymbol":false,
            "fiat":false,
            "pair":false
         },
         {
            "security":"ETHUSD",
            "fractionbase":100000,
            "pair_first":"ETH",
            "tradingsymbol":true,
            "fiat":false,
            "pair_second":"USD",
            "pair":true
         },
         {
            "security":"ETH",
            "fractionbase":100000,
            "tradingsymbol":false,
            "fiat":false,
            "pair":false
         },
         {
            "security":"BTCUSD",
            "fractionbase":100000,
            "pair_first":"BTC",
            "tradingsymbol":true,
            "fiat":false,
            "pair_second":"USD",
            "pair":true
         },
         {
            "security":"BTC",
            "fractionbase":100000,
            "tradingsymbol":false,
            "fiat":false,
            "pair":false
         },
         {
            "security":"BCHUSD",
            "fractionbase":100000,
            "pair_first":"BCH",
            "tradingsymbol":true,
            "fiat":false,
            "pair_second":"USD",
            "pair":true
         },
         {
            "security":"BCH",
            "fractionbase":100000,
            "tradingsymbol":false,
            "fiat":false,
            "pair":false
         }
      ],
         "type":"getsymbols"
   }

```

**Category:** Market Data<br />
**Permissions:** Public<br />
**Endpoint:** getsymbols

Retrieves the current assets and trading pairs on the platform.

### Response

| Key               | Value                                                        |
| -------------------- | ------------------------------------------------------------ |
| security           | **string**. The trading pair or asset. |
| fractionbase       | **string**. The number of decimals supported. e.g 100 = 1.00        |
| tradingsymbol      | **string**. true if a trading pair, false if an asset          |
| pair_first         | **string**. Specifies what the first asset is. e.g. LTC vs USD      |
| fiat               | **string**. true for fiat, false for virtual assets    |
| pair               | **string**. true if a trading pair, false if an asset       |



## Subscribe Order Book

> You can identify multiple trading pairs (security) by using '*".

```json
# Request

   {
      "type": "subscribe",
      "request": [
         {
            "msg":"book",
            "security":"BCHUSD",
            "dest":"CROX"
         }
      ]
   }

# Response

# order added

   {
      "security":"BCHUSD",
      "books":[
         {
            "side":"B",
            "act":"U",
            "src":"CROX",
            "price":"472.33",
            "qty":"0.46",
            "id":120947223715360,
            "time":"1626338416747",
            "mpid":"ANON",
            "key":"NA"
         }
         ],
         "type":"book"
   }

  # order removed

   {
      "security":"BCHUSD",
      "books":[
         {
            "act":"R",
            "id":120947223715360,
            "time":"1626338444101"
         }
            ],
               "type":"book"
   }

```

**Category:** Market Data<br />
**Permissions:** Public<br />
**Endpoint:** subscribe

Retrieves the latest order book information and then subscribes the user to ongoing market data event updates for the trading pairs specified.. 

The **Subscribe** call responds with the response shown below. Messages are then periodically sent in the same format as this response *UpdateEvent* information when best-bid/best-offer issue, until you close the connection.

### Request


| Key          | Value                                                        | Required |
| ------------ | ------------------------------------------------------------ | -------- |
| msg          | **string**. Specify the type of data to subscribe to. use 'book' for order book data. | Yes |
| security     | **string**. Specify the trading pair you wish to subscribe to. | Yes |
| dest         | **string**. This value will alway be set to CROX. | Yes |

### Response

| Key               | Value                                                        |
| -------------------- | ------------------------------------------------------------ |
| security           | **string**. The trading pair being subscribed to. |
| side               | **string**. The order side.        |
| act                | **string**. The act to Update or Remove orders   |
| price              | **string**. The price of the order.           |
| qty                | **string**. The size of the order.       |
| time               | **string**. The time of the order, in POSIX format.        |





## Subscribe Live Trades

> You  must specify a single trading pair for live trades.

```json

# Request

   {
      "type": "subscribe",
      "request": [
         {
            "msg":"trade",
            "security":"BCHUSD",
            "dest":"CROX"
         }
      ]
   }

# Response

   {
      "security":"BCHUSD",
      "src":"CROX",
      "price":"471.43",
      "qty":"0.06969",
      "time":"1626338546538",
      "type":"trade",
      "matchid":"62YNMDOQ2SPR"
   }

```

**Category:** Live Trades<br />
**Permissions:** Public<br />
**Endpoint:** subscribe

Retrieves the latest trade information and then subscribes the user to ongoing trade event updates for the specified trading pair. 

The **Subscribe** call responds with the response shown below. The Messages are then periodically sent in the same format as this response until you close the connection.


### Request


| Key          | Value                                                        | Required |
| ------------ | ------------------------------------------------------------ | ---------|
| msg          | **string**. Specify the type of data to subscribe to. use 'trade' for trade history data. | Yes |
| security     | **string**. Specify the trading pair you wish to subscribe to. | Yes |
| dest         | **string**. This value will alway be set to CROX. | Yes |

### Response

| Key               | Value                                                        |
| -------------------- | ------------------------------------------------------------ |
| security           | **string**. The trading pair being subscribed to. |
| src                | **string**. The value will always be set to CROX.
| price              | **string**. The price of the order.           |
| qty                | **string**. The size of the order.       |
| time               | **string**. The time of the order, in POSIX format.        |
| type               | **string**. The nature of record.  |
| matchid            | **string**. The exchange reference ID of the trade.   | 



## Query Trade History

> You  must specify a single trading pair for execution history.

```json
# Request

   {
      "type":"lshistory",
      "security":"ETHUSD",
      "starttime":"1630530000000"
   }


# Response

{
      "result": "OK",
      "security": "LTCUSD",
      "data": 
         [
               {
                  "security": "LTCUSD",
                  "rec_no": 1,
                  "execprice": "182.51",
                  "time": "1630561535942",
                  "execqty": "0.5702"
               }
         ],
            "total_rec": "1",
            "starttime": "1630530000000",
            "type": "lshistory"
   } 
```
**Category:** Trade History<br />
**Permissions:** Public<br />
**Endpoint:** lshistory

The query returns trade history data will return data from the specified start time. The “total_rec” tells how many records there should be, and “rec_no” tell you the current number of record.


### Request


| Key          | Value                                                        | Required |
| ------------ | ------------------------------------------------------------ | ---------|
| type         | **string**. Specify the type of data to subscribe to using 'tradehistory' for trade history data. | Yes |
| security     | **string**. Specify the trading pair you wish to subscribe to. | Yes |
| starttime    | **string**. Specify the start time in POSIX format | Yes |


### Response

| Key               | Value                                                        |
| -------------------- | ------------------------------------------------------------ |
| security           | **string**. The trading pair being subscribed to. |
| execprice          | **string**. The price of the order.           |
| execqty            | **string**. The size of the order.       |
| time               | **string**. The time of the order, in POSIX format.        |
| rec_no             | **int**. The record number.  |
| starttime          | **string**. The start time of the window.   |
| total_rec          | **string**. The total records displayed.    |



## Query Aggregate Trade History

> You  must specify a single trading pair for execution history.

```json
# Request

   {
      "type": "tradehistory",
      "security": "LTCUSD" 
   }

 
# Response

   {
      "result":"OK",
      "security":"LTCUSD",
      "data":[
         {
            "volume":"0.9375",
            "numoftrades":"1",
            "lastttime":"1630497372665",
            "blocktime":"1630497360000",
            "highprice":"174.69",
            "lowprice":"174.69",
            "rec_no":1,
            "starttime":"1630497372665",
            "lastprice":"174.69",
            "startprice":"174.69"
         }
      ],
         "total_rec":"1",
         "type":"tradehistory"
   }

```

**Category:** Trade History<br />
**Permissions:** Public<br />
**Endpoint:** tradehistory

The query returns the trade history in 1-minute block. If that minute has a trade, a corresponding block will be return with summary of that minute high/low/first/last trade price as well as volume and number of trades information.  

There can be multiple responses to one query if there are more than 100 entries in response. Each response will carry maximum of 100 entries. “total_rec” tells how many records there should be, and “rec_no” tell you current number of record.


### Request


| Key          | Value                                                        | Required |
| ------------ | ------------------------------------------------------------ | ---------|
| type         | **string**. Specify the type of data to subscribe to using 'tradehistory' for trade history data. | Yes |
| security     | **string**. Specify the trading pair you wish to subscribe to. | Yes |


### Response

| Key               | Value                                                        |
| -------------------- | ------------------------------------------------------------ |
| security           | **string**. The trading pair being subscribed to. |
| price              | **string**. The price of the order.           |
| qty                | **string**. The size of the order.       |
| time               | **string**. The time of the order, in POSIX format.        |
| numoftrades        | **string**. The number of trades.  |
| lastttime          | **string**. The last transaction time.   |
| blocktime          | **string**.    |
| high price         | **string**. The highest price of the window. |
| lowprice           | **string**. The Lowest price of the window.
| rec_no             | **int**. The record number.  |
| starttime          | **string**. The start time of the window.   |
| lastprice          | **string**. The last price of the window.   |
| startprice         | **string**. The start price of the window.  |
| total_rec          | **string**. The total records displayed.    |




## Subscribe Trading Pair Status

```json
# Request

   {
      "type": "subscribesymbolstatus"
   }

# Response

   {
      "result":"OK",
      "type":"subscribesymbolstatus"
   }

  # OR

   {
      "data": [
         {
            "security": "BCHUSD", 
            "time": "1623155345213", 
            "status": "Halt"
         }
      ],
         "type": "symbolstatus"
   }

  # OR

   {
      "data": [
         {
            "security": "BCHUSD", 
            "time": "1623155345213", 
            "status": "PreOpening"
         }
      ],
         "type": "symbolstatus"
   }

  # OR

   {
      "data": [
         {
            "security": "BCHUSD", 
            "time": "1623155345213", 
            "status": "Normal"
         }
      ],
         "type": "symbolstatus"
   }

```

**Category:** Trading Pair Status<br />
**Permissions:** Public<br />
**Endpoint:** subscribesymbolstatus

Retrieves any changes to trading pair status. Values can be Halt, PreOpening or Normal.. 

The **subscribesymbolstatus** call responds with the responses shown below. Messages are sent anytime the status is updated.
### Request
| Key          | Value                                                        | Required |
| ------------ | ------------------------------------------------------------ | ---------|
| type         | **string**. Specify the type of data to subscribe to using 'subscribesymbolstatus' for symbol status data. | Yes |
### Response

| Key                | Value                                                    |
| -------------------| -------------------------------------------------------- |
| security           | **string**. The trading pair being subscribed to.        |
| time               | **string**. The time of the order, in POSIX format.      |
| status             | **string**. The new status for the trading pair.         |


## Subscribe High and Low Price

> You can identify multiple trading pairs (security) by using '*".

```json
# Request

   {
      "type": "subscribehighlow",
      "security": "*"
   }

# Response

   {
      "security": "BTCUSD",
      "prevcloseprice": "35739.34", 
      "type": "highlow"
   }

# OR

   {
      "openingprice": "2388.1", 
      "security": "ETHUSD", 
      "highprice": "2388.1", 
      "lowprice": "2388.1", 
      "prevcloseprice": "2611.75", 
      "type": "highlow"
   }

# OR

   {
      "result": "OK", 
      "security": "*", 
      "type": "subscribehighlow"
   } 

```
**Category:** Market Data<br />
**Permissions:** Public<br />
**Endpoint:** subscribehighlow

Subscribes the user to ongoing market data event updates when there is a new high or low for the trading pairs specified. The time window 01:00 AM GST - 12:59 AM GST + 1

The **subscribehighlow** call responds with the response shown below. Messages are then periodically sent if there is a new high or low price.

### Request

| Key          | Value                                                        | Required |
| ------------ | ------------------------------------------------------------ | -------- |
| security     | **string**. Specify the trading pair you wish to subscribe to. | Yes |

### Response

| Key               | Value                                                        |
| -------------------- | ------------------------------------------------------------ |
| security           | **string**. The trading pair being subscribed to. |
| ppeningprice       | **string**. The price at 01:01 AM or price after an opening price calculation.        |
| highprice          | **string**. The highest execution price.           |
| lowprice           | **string**. The lowest execution price.       |
| prevcloseprice     | **string**. The execution price at 01:00 AM        |




# Authentication

**Category:** Authentication<br />
**Permissions:** Trading<br />

Once client establishes the WebSocket connection, the client has 30 seconds to complete the login process. Otherwise, the socket will be closed by Ousoul.



## Challenge
```json
# Request

   {
      "type": "challenge"
   }


# Response

   {
      "result":"OK",
      "type":"challenge",
      "key":"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCvBXTe278Lwg2MoI7iGKolSYuF+sNFKrsZplxCN9x0kItU3KIf8+1q60ILLwLewCEf7foxzpWp32j9YYU9vNBghuJ7BHcDYTffTRcv+QdNno491j701Hq7DIw13AGCQQTRcnfclvblnytIEWoQsiUvPJcdiWgqJIX3IQGA47a+uwIDAQAB"
   }

```

First client needs to send a challenge request to the system.

### Response

| Key               | Value                                                        |
| -------------------- | ------------------------------------------------------------ |
| Key                  | **string**. The string used to encrypt the password |


>  EXAMPLE.

```java
   byte[] array = javax.xml.bind.DatatypeConverter.parseBase64Binary(key);
   KeyFactorykf = KeyFactory.getInstance(“RSA”);
   publicKey = kf.generatePublic(new X509EncodedKeySpec(array));
   Cipher = Cipher.getInstance(“RSA”);
   cipher.init(Cipher.ENCRYPT_MODE, publicKey);
   byte[] result = cipher.doFinal ("PASSWORD".getBytes(“UTF-8”));
   String output = javax.xml.bind.DatatypeConverter.printBase64Binary(result);
```

```python

import websocket
import json
import rsa
import base64 

def websocket_login(ws, username, passphrase):
    ws.send('{"type":"challenge"}')
    publicKey       = rsa.PublicKey.load_pkcs1_openssl_der(base64.b64decode(json.loads(ws.recv())['key']))
    passPhrase      = rsa.encrypt(passphrase.encode(), publicKey)
    passPhrase      = base64.b64encode(passPhrase)
    ws.send(str({"type":"login","userid": username,"pass": passPhrase.decode('utf-8')}))
    return ws
ws = websocket.create_connection("wss://sandbox.ousoul.com/ws/")
websocket_login(ws,TRADER,PASSPHRASE)

```

```javascript
global.window = {};
const WebSocket = require('ws');
const JSEncrypt = require("jsencrypt");
const ws = new WebSocket("wss://sandbox.ousoul.com/ws/")

ws.onopen = function () {
    ws.send(JSON.stringify(
        {
            "type": "challenge"
        }

    ));
    ws.onmessage = function (response) {
        let websocket_response = JSON.parse(response.data);
        const key = websocket_response['key'];
        const encrypt = new JSEncrypt();
        encrypt.setPublicKey(key);
        const sign = encrypt.encrypt('PASSPHRASE');
        ws.send(JSON.stringify({
            "type": "login",
            userid: "TRADER",
            pass: sign
        }));

    }
}
```

The response contains a “key” field, which contains a public key string the client must use to encrypt the password field. The “key” field is a 64 base ASCII print out of the binary byte array for the corresponding public key of asymmetric public/ private key pair. Ousoul will hold the private key while sending clients the public key. Client will first convert the ASCII back to binary byte array. Then use that binary byte array to create a “RSA” public key instance. Afterwards, client will use that RSA public key instance to encrypt info required in this order entry API, e.g. password encryption.

**You need to use ```npm``` package to install ```jsencrypt``` library if you are using ``JavaScript``**.<br />
**```global.window = {};``` line only needed if you run the js script from a server**.


## Login
```json

# Request
   {
      "type":"login",
      "userid":"USERNAME",
      "pass":"s7UW26iGE/iVfk2ihPYIcyzRqZRi/Ztb23UNMomf3xrBzGKUHKzfNwZe5PIR/0zvfevYvkJnKLQVhR4U9/kObD/Ir0z6mBfLLgFwEcRm08jYI/nk7lDU+W32PqduTOCThlkXYueQslK54vR9rKvMs="
   }

# Response - You will get a 'result':'OK'

   {
      "result":"OK",
      "firm":"MIDC",
      "need2FA":true,
      "roles":"OOOOO",
      "active":"Y",
      "secondary_account":"TESEGY0101PGHA12345",
      "type":"login",
      "attr":
         {
            "country":"",
            "tax_code":"SR",
            "last_name":"USER",
            "first_name":"USER ",
            "email":"user@mail.com"
         },
            "use2fa":"Y",
            "userid":"user@mail.com",
   }

   # Any open orders will be returned as follows

   {
      "refno":"62YNMDOQ2SPSC0",
      "side":"B",
      "orderstatus":"Open",
      "type":"order",
      "userid":"user@mail.com",
      "execamount":"0",
      "tif":"GTC",
      "firm":"MIDC",
      "security":"BCHUSD",
      "price":"471.43",
      "liveqty":"0.49031",
      "qty":"0.49031",
      "updtime":"1626342626369",
      "enttime":"1626342626366",
      "category":"STAGE",
      "execqty":"0",
      "account":"user@mail.com",
      "ordertype":"LMT"
   }

   # Your current positions will also be returned

   {
      "firm":"MIDC",
      "security":"USD",
      "curpos":"1013057.59",
      "cost":"-841663670.1899977",
      "type":"position",
      "account":"user@mail.com"
   }

   {
      "firm":"MIDC",
      "security":"BCH",
      "curpos":"128.25376",
      "cost":"12008212.15",
      "type":"position",
      "account":"user@mail.com"
   }

   {
      "firm":"MIDC",
      "security":"LTC",
      "curpos":"121.7144",
      "cost":"1199785.55",
      "type":"position",
      "account":"user@mail.com"
   }

# OR

   {
      "result":"invalid user/password",
      "type":"login"
   }
```

After encryption of password, the next step is to send in the login request. The string in the “pass” field is from the output result in sample above.

| Key               | Value                                                        | Required |
| -------------------- | ------------------------------------------------------------ | ----- |
| userid                  | **string**. The username of the user. | Yes |
| pass                  | **string**. The pass phrase generated from the challenge and steps above. | Yes |

### Response

| Key               | Value                                                        |
| -------------------- | ------------------------------------------------------------ |
| result                  | **string**. The reposne will either contain OK if successful or a rejection reason. |

<br/>
Ousoul will send current open orders and position information upon login. If a user places a new order, Ousoul will send an order update automatically. If the user trades, they will receive a trade update and corresponding position updates.



## Logout

```json
# Request
   {
      "type":"logout",
   }

# Response

   {
      "result":"OK",
      "type":"logout"
   }
```

This is for clean logout. Once Ousoul receives this message, it will immediately terminate the WebSocket connection.


# Order Placement

**Category:** Trading<br />
**Permissions:** Trading<br />

You can place two types of orders: limit and market. Orders can only be placed if your account has sufficient funds.

## Addorder

```json

# Request - LTCUSD Buy Market Order IOC order for 180 dollars.

   {
      "type":"addorder",
      "security":"LTCUSD",
      "side":"B",
      "ordertype":"MKT",
      "tif":"IOC",
      "amount":"180"
   }


# Response 

   # If the order is places successfully you will as "result":"OK" response , followed by any 
     order , execution and position updates.


   {
      "tif":"IOC",
      "result":"OK",
      "firm":"MIDC",
      "refno":"62YNMDOQ2SPIC0",
      "security":"LTCUSD",
      "side":"B",
      "amount":180,
      "type":"addorder",
      "userid":"user@mail.com",
      "ordertype":"MKT"
   }

   {
      "refno":"62YNMDOQ2SPIC0",
      "side":"B",
      "amount":"180",
      "orderstatus":"Open",
      "type":"order",
      "userid":"user@mail.com",
      "execamount":"0","tif":"IOC",
      "firm":"MIDC",
      "security":"BCHUSD",
      "price":"0",
      "liveqty":"0.00001",
      "qty":"0.00001",
      "updtime":"1626332766266",
      "enttime":"1626332766266",
      "category":"STAGE",
      "execqty":"0",
      "account":"user@mail.com",
      "ordertype":"MKT"
   }

   {
      "firm":"MIDC",
      "security":"LTC",
      "curpos":"0.38031",
      "cost":"17999691.99",
      "type":"position",
      "account":"user@mail.com"
   }

   {
      "traderefno":"62YNMDOQ2SPIC02_62YNMDOQ2SPJ",
      "refno":"62YNMDOQ2SPIC0",
      "trdtime":"1626332766360",
      "side":"B",
      "ismaker":false,
      "execprice":"183.29",
      "type":"sale",
      "userid":"user@mail.com",
      "firm":"MIDC",
      "security":"LTCUSD",
      "exchangeref":"62YNMDOQ2SPJ",
      "category":"CROX",
      "execqty":"0.38031",
      "account":"user@mail.com"
   }

   {
      "firm":"MIDC",
      "security":"USD",
      "curpos":"622.09",
      "cost":"-11216742.005",
      "type":"position",
      "account":"user@mail.com"
   }

   {
      "refno":"62YNMDOQ2SPIC0",
      "side":"B",
      "amount":"180",
      "orderstatus":"Executed",
      "type":"order",
      "userid":"user@mail.com",
      "execamount":"179.9969199",
      "tif":"IOC",
      "firm":"MIDC",
      "security":"LTCUSD",
      "price":"0",
      "liveqty":"0",
      "qty":"0.38031",
      "updtime":"1626332766920",
      "enttime":"1626332766266",
      "category":"STAGE",
      "execqty":"0.38031",
      "account":"user@mail.com",
      "ordertype":"MKT"
   }



   # If the order is not submitted successfully , you will see the reject reason 
     in the result field.


   # Post/Maker order entered at price that will execute

   {
      "tif":"GTC",
      "result":"order marketable to existing quote/book",
      "firm":"MIDC",
      "security":"LTCUSD",
      "side":"B",
      "alo":true,
      "price":"173.29",
      "qty":"0.29969",
      "type":"addorder",
      "userid":"user@mail.com",
      "ordertype":"LMT",
      "clientorderi": "7599bb88-1d48-48b1-bf90-ed20d81b344d"
   }

   {
      "tif":"GTC",
      "result":"order marketable to existing quote/book",
      "firm":"MIDC",
      "security":"LTCUSD",
      "side":"B",
      "alo":true,
      "price":"173.29",
      "qty":"0.29969",
      "type":"addorder",
      "userid":"user@mail.com",
      "ordertype":"LMT"
   }

   # Passing incorrect field

   {
      "result":"cannot use qty for market buy order",
      "tif":"IOC",
      "ordertype":"MKT",
      "security":"LTCUSD",
      "side":"B",
      "qty":"180",
      "type":"addorder"
   } 

```

You can place two types of orders: limit and market. Orders can only be placed if your account has sufficient funds.

### Request

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| security           | **string**. The trading pair you wish to place the order in. | Yes |
| side               | **string**. The order side. Options are buy 'B' or sell 'S'. | Yes |
| price              | **string**. Specify the order price. Required for limit orders | Conditionally |
| amount             | **string**. Specify the amount in fiat for the order. Required for market buy orders. | Conditionally |
| ordertype          | **string**. Specify if sending a limit or market order. Options are 'LMT' or 'MKT'. Default is 'LMT'.  |
| tif                | **string**. Options are GTC, IOC, FOK, GTT. GTC is the default | No |

### Response

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| security           | **string**. The trading pair you wish to place the order in. | Yes |
| side               | **string**. The order side. Options are buy 'B' or sell 'S'. | Yes |
| qty                | **string**. Specify the quantity of order. Required for all orders except market buy orders. | Conditionally |
| price              | **string**. Specify the order price. Required for limit orders | Conditionally |
| amount             | **string**. Specify the amount in fiat for the order. Required for market buy orders. | Conditionally |
| ordertype          | **string**. Specify if sending a limit or market order. Options are 'LMT' or 'MKT'. Default is 'LMT'.  |
| tif                | **string**. Options are GTC, IOC, FOK, GTT. GTC is the default | No |
| stoptype           | **string**. Used to place a stop limit ot stop market order by setiing value to 'STOP'. Default is not set. | No |
| stopprice          | **string**. Specifiy the stop price if stop type is set. | Conditionally |
| exptime            | **string**. The expiry time of the order, in POSIX format. Required if TIF is set to GTT. | Conditionally |
| alo                | **boolean**. The alo (Add Liquidity Only) field can be set to true for post/maker only orders | No |
| clientorderid      | **string**. You can pass your own client order id which will be echoed back in corresponding “order”,” sale” updates.

<br />

Rejected orders will be displayed with reasons. Please refer to the <a href="https://docs.exchange.ousoul.com/#reject-messages">Reject Messages</a> section for the various types of error messages returned when triggered.




## Cancelorder
```json
# Request

   {
      "type":"cancelorder",
      "security":"BTCUSD",
      "refno":"5FSDIAGCE768A0"
   }

# Response

   {
      "result":"OK",
      "refno":"5FSDIAGCE768A0",
      "security":"BTCUSD",
      "type":"cancelorder"
   }

# OR
   
   {
      "result":"order not found",
      "refno":"5FSDIAGCE768A0",
      "security":"BTCUSD",
      "type":"cancelorder"
   }

# OR
   
   {
      "result":"failed",
      "refno":"5FSDIAGCE768A0",
      "security":"BTCUSD",
      "type":"cancelorder"
   }
```

To cancel an open order, you can use the cancelorder endpoint. You must pass the Ousoul assigned 'refno' to specify what order to cancel.

### Request

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| security           | **string**. The trading pair you wish to cancel the order for. | Yes |
| refno              | **string**. The Ousoul generated order id. | Yes |

### Response

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| result             | **string**. The reposne will either contain OK if successful or a rejection reason. |
| security           | **string**. The trading pair you wish to cancel the order for. | - |
| refno              | **string**. The Ousoul generated order id. | - |



# Order & Trade Query

**Category:** Trading<br />
**Permissions:** Trading<br />

Users can retrieve open and executed order information.

## Queryorder

```json

# Request

   {
      "type":"queryorder",
      "security":"BTCUSD",
      "refno":"5GS08WTIE8CWA0"
   }

# Response

   {
      "result":"OK",
      "refno":"5GS08WTIE8CWA0",
      "data": {
         "updtime":"1557505235085",
         "tif":"GTC",
         "execamount":"10000",
         "qty":"1000",
         "security":"BTCUSD",
         "type":"order",
         "clientorderid":"ID1",
         "liveqty":"900",
         "category":"CROX",
         "refno":"5GS08WTIE8CWA0",
         "price":"100",
         "execqty":"100",
         "side":"S"
      },
      "security":"BTCUSD",
      "type":"queryorder"
   }

# OR
   
   {
      "result":"order not found",
      "refno":"5GS08WTIE8CWA0",
      "security":"BTCUSD",
      "type":"queryorder"
   } 

```


Users can retrieve open and executed order information.

### Request

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| security           | **string**. The trading pair you wish to query orders for. | Yes |
| refno               | **string**. The Ousoul generated order id. | Yes |

### Response

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| updtime           | **string**. Time of the last order update, in POSIX format. | - |
| tif               | **string**. The order's time in force. | - |
| execamount        | **string**. The amount of the order that has executed in fiat value. | - |
| qty               | **string**. The order's original quantity. | - |
| security          | **string**. The trading pair. | - |
| liveqty           | **string**. The order's remaining open quantity. | - |
| refno             | **string**. The Ousoul generated order id. | - |
| execqty           | **string**. The order quantity that has been executed. | - |
| side              | **string**. The order side | - |



## Openorderquery

```json

# Request

   {
      "type":"openorderquery",
      "security":"ETH"
   }

   # OR to Query all open orders for all Assets

   {
      "type":"openorderquery",
      "security":"*"
   }

# Response 

   {
      "result": "OK",
      "security": "*",
      "data": [
         {
               "refno": "66ZHSAPJYL3DC1",
               "side": "B",
               "orderstatus": "Open",
               "type": "order",
               "userid": "user@mail.com",
               "execamount": "0",
               "tif": "GTC",
               "firm": "MIDC",
               "commrate": "0|40",
               "security": "ETHUSD",
               "price": "4000",
               "liveqty": "1",
               "rec_no": 1,
               "qty": "1",
               "updtime": "1638860161388",
               "enttime": "1638860161388",
               "category": "STAGE",
               "execqty": "0",
               "account": "user@mail.com",
               "ordertype": "LMT"
         },
         {
               "refno":"66ZHSAPJYPOFC0",
               "side": "B",
               "orderstatus": "Open",
               "type": "order",
               "userid": "user@mail.com",
               "execamount": "0",
               "tif": "GTC",
               "firm": "MIDC",
               "commrate": "0|40",
               "security": "BTCUSD",
               "price": "50000",
               "liveqty": "1",
               "rec_no": 2,
               "qty": "1",
               "updtime": "1638860142071",
               "enttime": "1638860142071",
               "category": "STAGE",
               "execqty": "0",
               "account": "user@mail.com",
               "ordertype": "LMT"
         }
      ],
      "total_rec": 2,
      "type": "openorderquery"
   }

   # OR if there are no records

   {
    "result": "OK", 
    "total_rec": 0, 
    "type": "openorderquery"
   }

```

This query for getting open orders for all Assets all or certain Asset.

### Request

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| Security           | **string**. The trading pair you wish to query trades for. | No |


### Response

“total_rec” tells you how many records there are available.

If there are more than 100 records, the system will send you multiple response, each will have 100 records.

| Key               | Value                                  | Required |
| ----------------- | ----------------------------------- | -------- |
| Security          | **string**. The trading pair. | - |
| Refno             | **string**. The Ousoul generated order id. | - |
| Execqty           | **string**. The order quantity that has been executed. | - |
| rec_no            | **int**. The record number of the response. | - |
| Side              | **string**. The order side | - |
| price             | **string**. The price of the order.      | - |
| qty               | **string**. The size of the order.       | - |
| ordertype         | **string**. Specify if sending a limit or market order. Options are 'LMT' or 'MKT'. Default is 'LMT'. | - |
| orderstatus       | **string**. The status of the order (Open, Executed, Rejected, Cancelled) | - |
| commrate          | **string**. The comission rate (E.G If the comission rate is 0/40 this meaning 0 for maker , 40 basis points for taker).  | - |
| liveqty           | **string**. The order's remaining open quantity. | - |
| updtime           | **string**. Time of the last order update, in POSIX format. | - |
| enttime           | **string**. The time order was entered in POSIX format. | - |


## Querytrade

```json

# Request

   {
      "type":"querytrade",
      "security":"LTCUSD",
      "fromtime":"1557323619303"
   }

   # OR

   {
      "type": "querytrade",
      "maxreturn": 1000, 
      "lastfirst": true, 
      "userid": "*", 
      "fromtime": "1633305600000"
   }

# Response

   {
      "result":"OK",
      "data":[
         {
            "account": "user@mail.com",
            "refno":"5GQBE9BCZOO0A3",
            "category":"CROX",
            "comm": "0.00001",
            "commsecurity": "LTC",
            "exchangeref": "648KTXPYN46U",
            "execqty":"10",
            "execprice":"30",
            "ismaker": false,
            "traderefno":"5GQBE9BCZOO0A32_5GQBE9BCZOO2",
            "side":"B",
            "trdtime":"1557323619303",
            "rec_no":1,
            "security":"LTCUSD",
            "type":"sale"
         },
         {
            "account": "user@mail.com",
            "refno":"5GQBE9BCZN4GA7",
            "category":"CROX",
            "exchangeref": "648KTXPYN46Z",
            "execqty":"10",
            "execprice":"100.5",
            "ismaker": true,
            "traderefno":"5GQBE9BCZN4GA72_5GQBE9BCZN4I",
            "side":"B",
            "trdtime":"1557323603427",
            "rec_no":2,
            "security":"LTCUSD",
            "type":"sale"
         }
      ],
      "type":"querytrade",
      "total_rec":2
   }


```

This is to query the trades for a user , or specify certain amount of order query you wish to get and from sepecific time.

### Request

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| Security           | **string**. The trading pair you wish to query trades for. | No |
| maxreturn          | **string**. Specify the maxmuim amount of orders returned. | No |
| lastfirst          | **string**. Specify that you wish to return last orders. | No |
| userid             | **string**. Specify the user id | No |
| Fromtime           | **string**. Specify the cut off time in POSIX format. | No |

### Response

“total_rec” tells you how many records there are available.

If there are more than 100 records, the system will send you multiple response, each will have 100 records.

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| Trdtime           | **string**. The execution time, in POSIX format. | - |
| Execprice         | **string**. The execution price of the trade. | - 
| Security          | **string**. The trading pair. | - |
| Refno             | **string**. The Ousoul generated order id. | - |
| Traderefno        | **string**. The Ousoul generated execution id. | - |
| Execqty           | **string**. The order quantity that has been executed. | - |
| Rec_no            | **int**. The record number of the response. | - |
| Side              | **string**. The order side | - |


# Trading Transactions

**Category:** Tracking Transactions<br />
**Permissions:** Trading<br />

## Querydeposit

```json

# Request

   {
      "type":"querydeposit",
      "maxreturn": 1000, 
      "lastfirst": "true", 
      "fromtime": "1639406640393",
      "userid": "user@mail.com"
   }


# Response

   {
      "result":"OK",
      "data":
      [
         {
            "firm":"TFF1",
            "traderefno":"STAGEWEBSOCKET:user@mail.com:1637313057457:1052",
            "security":"BTC",
            "amount":"0.00001",
            "rec_no":1,
            "time":"1637313057457",
            "type":"withdraw",
            "userid":"user@mail.com",
            "account":"user@mail.com",
            "hash":"STAGEWEBSOCKET:user@mail.com"
         }
      ],
      "total_rec": 1,
      "type": "querydeposit"
   }

```


This is to query the transations history for a user.

### Request

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| maxreturn          | **string**. Specify the maxmuim amount of orders returned. | No |
| lastfirst          | **string**. Specify that you wish to return last orders. | No |
| userid             | **string**. Specify the user id | No |
| Fromtime           | **string**. Specify the cut off time in POSIX format. | No |

### Response

“total_rec” tells you how many records there are available.

If there are more than 100 records, the system will send you multiple response, each will have 100 records.

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| time               | **string**. The time of transaction. | - |
| security           | **string**. The trading pair. | - |
| type               | **string**. The type of Transction | - |
| traderefno         | **string**. The Ousoul generated execution id.  | - |
| amount             | **string**. the amount of Cryptocurrency transferred. | - |
| hash               | **int**. The transaction id. | - |
| rec_no             | **int**. The record number of the response. | - |

# Account Balances

**Category:** Balances and Positions<br />
**Permissions:** Trading<br />

## Querypos

```json

# Request

   {
      "type":"querypos",
      "security":"LTC"
   }

# Resposne

   {
      "result":"OK",
      "firm":"MIDC",
      "data": [
         {
            "curpos":"300",
            "rec_no":1,
            "security":"LTC",
            "type":"position"
         },
         {
            "curpos":"300",
            "rec_no":2,
            "security":"USD",
            "type":"position"
         }
      ],
      "userid":"USER",
      "type":"querypos",
      "total_rec":2
   }

# OR

   {
      "result":"no position",
      "type":"querypos"
   } 

```

Users can receive balance information, positions and continuous updates afterwards.

### Request

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| security           | **string**. The virtual asset or currency to query for. | No |

### Response

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| curpos           | **string**. The amount of that Virtual Asset or Fiat in the users balance | - |
| security          | **string**. The virtual asset or currency to query for. | - |


## Querypendingpos

```json

# Request

   {
      "type":"querypendingpos"
   }

# Resposne

   {
      "result": "OK", 
      "data": [
         {
            "security": "ETH", 
            "curpos": "1.13361", 
            "rec_no": 1, 
            "managedpos": "0", 
            "type": "position", 
            "openpos": "0"
         }, 
         {
            "security": "BCH", 
            "curpos": "0.14721", 
            "rec_no": 2, 
            "managedpos": "0.1", 
            "type": "position", 
            "openpos": "0"
         }, 
         {
            "security": "USD", 
            "curpos": "1269.59", 
            "rec_no": 3, 
            "managedpos": "0", 
            "type": "position", 
            "openpos": "0"
         }
      ], 
      "total_rec": 3, 
      "type": "querypendingpos"
   } 

# OR

   {
      "result":"no position",
      "type":"querypendingpos"
   } 

```


Note: This endpoint does not auto update and has to be manually queried. Only querypos will continuously update when balance changes.

### Request

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| security           | **string**. The virtual asset or currency to query for. | No |

### Response

| Key                | Value                                  | Required |
| -----------------  | ----------------------------------- | -------- |
| curpos           | **string**. The amount of that Virtual Asset or Fiat in the users balance. | - |
| security         | **string**. The virtual asset or currency to query for. | - |
| rec_no           | **int**. The record number. | - |
| managedpos       | **string**. The withheld balance or The amount withheld in open orders. | - |
| openpos          | **string**. The field can be ignored and will always be zero. | - |



# Reject Messages

You may receive different reject messages while attempting to place orders.The following is a list of error messages that are returned by the system when risk rejects are triggered. 

**Order marketable to existing quote/book**
<br />
This error message indicates that you are attempting to place orders that will execute against other orders in the order book in the case of post only orders (add only orders).<br />

**Sample** 
<br />
``{"tif":"GTC","result":"order marketable to existing quote/book","firm":"MIDC","security":"BCHUSD","side":"B","alo":true,"price":"473.29","qty":"0.29969","type":"addorder","userid":"user@mail.com","ordertype":"LMT"}``<br />
<br />

**Exceed order/sec limit**
<br />
This error message indicates that you are attempting to place orders beyond the maximum order quantity per second (more than 4 orders per second).
<br />

**Sample**
<br />
``{'tif': 'GTT', 'result': 'exceed order/sec limit', 'exptime': '1631004134000', 'security': 'ETHUSD', 'side': 'B', 'alo': 'true', 'price': '3782.01', 'qty': '0.0825', 'type': 'addorder', 'clientorderid': '7599bb88-1d48-48b1-bf90-ed20d81b344d'}``<br />
<br />


**No crypto pos**
<br />
This error message indicates that you are attempting to place orders with insufficient virtual asset balance.
<br />

**Sample**
<br />
``{"tif":"IOC","result":"no crypto pos","security":"BTCUSD","side":"B","amount":"5100","type":"addorder","ordertype":"MKT"}
{"refno":"64GSC7W0XIY8C0","side":"B","amount":"5100","orderstatus":"Rejected","type":"order","userid":"user@mail.com","execamount":"0","tif":"IOC","firm":"9737ARECYP","security":"BTCUSD","price":"0","liveqty":"0","qty":"0","updtime":"1631017300225","text":"no crypto 
pos","enttime":"1631017300225","category":"STAGE","execqty":"0","account":"user@mail.com","ordertype":"MKT"}``<br />
<br />


**Wash sale prohibited**
<br />
This error message indicates that you are attempting to place an order that will execute against another order you have placed on the opposite side of the order book.
<br />

**Sample**
<br />
``{"refno":"64GSC7W0XHR8C0","side":"B","amount":"510","orderstatus":"Rejected","type":"order","userid":"user@mail.com","execamount":"0","tif":"IOC","firm":"9737ARECYP","security":"BTCUSD","price":"0","liveqty":"0","qty":"0","updtime":"1631016705668","text":"wash sale prohibited","enttime":"1631016705668","category":"STAGE","execqty":"0","account":"user@mail.com","ordertype":"MKT"}``<br />
<br />


**Exceeds margin limit Exceed total Amount**
<br />
This error message indicates that you are attempting to place an order that is above the maximum daily notioanl value.
<br />

**Sample**
<br />
``{"tif":"GTC","result":"Exceeds margin limit Exceed total Amount","security":"BTCUSD","side":"B","price":"46500","qty":"100","type":"addorder","ordertype":"LIMIT"}
{"refno":"64IEBHMP914LC0","side":"B","orderstatus":"Rejected","type":"order","userid":"user@mail.com","execamount":"0","tif":"GTC","firm":"MIDC","security":"BTCUSD","price":"46500","liveqty":"0","qty":"100","updtime":"1631175016204","text":"Exceeds margin limit Exceed total Amount","enttime":"1631175016204","category":"STAGE","execqty":"0","account":"user@mail.com","ordertype":"LMT"}``<br />
<br />


**Error parsing json**
<br />
This error message indicates that there are typographical errors in your request. 
<br />

**Sample**
<br />
``{"result":"error parsing json"}``<br />
<br />